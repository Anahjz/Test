//
//  MoviesViewController.m
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MoviesViewController.h"
#import "MovieCell.h"
#import "GoMovies-Swift.h"

@interface MoviesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation MoviesViewController
-(void)viewDidLoad
{
    [super viewDidLoad];
    [self loadMovies];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.movies.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MovieCell *cell = [tableView dequeueReusableCellWithIdentifier:@"movieCell" forIndexPath:indexPath];
    NSObject* m = self.movies[indexPath.row];
    Movies*mv = (Movies*)m;
    cell.movie = mv ;
    return cell;
}

-(void) loadMovies
{[[Api getInstance]moviesListWithSuccess:^(NSArray * _Nonnull movies) {
    self.movies = movies;
    [self.tableview reloadData];
    
} failure:^(NSError * _Nullable error) {
    
}];
    
}
@end
