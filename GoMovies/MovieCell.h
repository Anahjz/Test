//
//  MovieCell.h
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Movies.h"

@interface MovieCell : UITableViewCell
@property(nonatomic) Movies *movie;
@end
