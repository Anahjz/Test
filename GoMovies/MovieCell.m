//
//  MovieCell.m
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieCell.h"

@interface MovieCell ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *movieImage;
@property (weak, nonatomic) IBOutlet UILabel *rting;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *genres;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *summary;
@property (weak, nonatomic) IBOutlet UILabel *likes;
@property (weak, nonatomic) IBOutlet UILabel *comments;

@end

@implementation MovieCell
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupView];
}

-(void) setupView
{
    self.bgView.layer.cornerRadius = 3;
    self.movieImage.layer.cornerRadius = 3;
    self.rting.layer.masksToBounds = true;
    self.rting.layer.cornerRadius = 14.0;
}

-(void)setMovie:(Movies *)movie
{
    _movie = movie;
    self.name.text = self.movie.name;
    self.rting.text =  [[NSNumber numberWithFloat:self.movie.rating] stringValue];
    self.genres.text = [self.movie.geners componentsJoinedByString:@","];
    self.time.text = [NSString stringWithFormat:@"%lum", (unsigned long)self.movie.time];
    self.summary.text = self.movie.summary;
    self.likes.text = @"12555";
    self.comments.text = @"12623";
    [self loadImage:self.movie.image];
}

-(void) loadImage:(NSString*)imageUrl
{
dispatch_async(dispatch_get_global_queue(0,0), ^{
    NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imageUrl]];
    if ( data == nil )
        return;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.movieImage.image = [UIImage imageWithData: data];
    });
});
}
@end
