//
//  Movies.h
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface Movies : NSObject
@property(nonatomic) NSInteger movieId;
@property(nonatomic) NSString *name;
@property(nonatomic) NSArray *geners;
@property(nonatomic) NSString *summary;
@property(nonatomic) NSString *image;
@property(nonatomic) NSUInteger time;
@property(nonatomic) double rating;
+ (Movies *)createFromDictionary:(NSDictionary*) dic;
@end
