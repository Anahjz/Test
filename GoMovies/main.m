//
//  main.m
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
