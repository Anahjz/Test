
//  HttpApi.swift
//  Takhfifan-Merchant
//
//  Created by admin on 8/6/17.
//  Copyright © 2017 Erfan Seyyedi. All rights reserved.
//

import Foundation

//HTTP Methods
enum HttpMethod : String {
    case  GET
    case  POST
    case  DELETE
    case  PUT
}


class HttpClientApi: NSObject
{
    var request : URLRequest?
    var session : URLSession?
    
    
    func makeAPICall(path:String ,params: Dictionary<String, Any>? ,method: HttpMethod ,success:@escaping ( Data? ,HTTPURLResponse?, NSError? ) -> Void, failure: @escaping ( Data? ,HTTPURLResponse?  , NSError? )-> Void)
    {
        let url = URL(string:"https://api.tvmaze.com/\(path)")
        session = URLSession.shared
        request = URLRequest(url: url!)
        request?.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        request?.httpMethod = method.rawValue
        
        do {
            if method != .GET {
                request?.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            }
        }
        catch let error
        {
            print(error.localizedDescription)
        }
        request?.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request?.setValue("application/json", forHTTPHeaderField: "Accept")

        
        session?.dataTask(with: request! as URLRequest) { (data, response, error) -> Void in
            if let data = data {
                if let response = response as? HTTPURLResponse
                {
                    if 200...299 ~= response.statusCode {
                        success(data , response , error as NSError?)
                    }
                    else {
                        let error  = NSError(domain: "ApiError",code: response.statusCode)
                        failure(data , response, error)
                    }
                }
                else {
                    failure(data , nil, error as NSError?)
                }
            }
            else {
                failure(data , response as? HTTPURLResponse, error as NSError?)
            }
            }.resume()
    }
}
