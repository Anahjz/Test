//
//  Movies.m
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Movies.h"

@implementation Movies
+ (Movies *)createFromDictionary:(NSDictionary*) dic
{
    Movies *movie = [Movies new];
    movie.name = (dic[@"name"] == [NSNull null]) ? nil :dic[@"name"];
    movie.movieId = (dic[@"id"] == [NSNull null]) ? 0 : [dic[@"id"] integerValue];
    movie.summary = (dic[@"summary"] == [NSNull null]) ? nil :dic[@"summary"];
    movie.geners = (dic[@"genres"] == [NSNull null]) ? @"-" :dic[@"genres"];
    movie.image =  dic[@"image"][@"medium"] ;
    NSDictionary*rating = dic[@"rating"];
    double average = (rating[@"average"] == [NSNull null]) ? 0.0 : [rating[@"average"] floatValue];
    movie.rating = average ;
    movie.time = [dic[@"runtime"] integerValue];
    
    return movie;
}
@end
