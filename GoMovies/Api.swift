//
//  Api.swift
//  GoMovies
//
//  Created by Apple on 12/29/17.
//  Copyright © 2017 Alibaba. All rights reserved.
//

import Foundation

@objc public class Api : NSObject, URLSessionDelegate
{
    private static let instance = Api()
    
    public static func getInstance() -> Api
    {
        return .instance
    }
    
    public func moviesList(success:@escaping ( _ movies:NSArray ) -> Void ,failure: @escaping ( NSError? )-> Void){
        HttpClientApi().makeAPICall(path:"shows" ,params:[:], method: HttpMethod.GET ,success: { (data, response, error) in
            do {
                if let movies = try JSONSerialization.jsonObject(with: data!, options: []) as? [NSDictionary] {
                    var result = [Movies]()
                    for movie  in movies
                    {
                        result.append(Movies.create(from:movie as! [AnyHashable : AnyObject]))
                    }
                    DispatchQueue.main.async { success(result as [Movies] as NSArray ) }
                }
            }
            catch {
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(String(describing: jsonStr))'")
                DispatchQueue.main.async { failure(error as NSError?) }
            }
        } , failure: { (data, response, error) in
            DispatchQueue.main.async { failure(error as NSError?) }
        }
        )

    }
}
